#!/bin/sh

find dist -name '*.pkg.tar.zst' -printf '%P\n' >packages.list
assets=""

while read x; do
  curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "./dist/${x}" "${ASSETS_URL}/${x}"

  assets+=" --assets-link {\"name\":\"${x}\",\"url\":\"${ASSETS_URL}/${x}\"}"
done <packages.list

release-cli create \
  --name "$CI_COMMIT_TAG" \
  --tag-name "$CI_COMMIT_TAG" \
  --description "$CI_PROJECT_NAME Arch Linux AUR Package" \
  ${assets}
