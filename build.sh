#!/bin/sh

set -ex

# install common dependencies
pacman -Syu base-devel sudo acl \
  --cachedir "$(realpath .paccache)" \
  --noconfirm

# create a non-root user to build and install the package
useradd -m makepkg
setfacl -m u:makepkg:rwx .
tee /etc/sudoers.d/makepkg_nopasswd <<EOF
makepkg ALL=(ALL:ALL) NOPASSWD: /usr/bin/pacman
EOF

# import required PGP keys, if any
sudo -u makepkg gpg --import keys.gpg || echo "no PGP keys to import"
sudo -u makepkg PKGDEST=${PKGDEST} makepkg -s --noconfirm

# find and install all packages from this build
pacman -U $(find dist -name '*.pkg.tar.zst') \
  --cachedir "$(realpath .paccache)" \
  --noconfirm
